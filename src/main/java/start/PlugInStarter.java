package start;


import ij.IJ;
import ij.ImageJ;

/**
 * Launch Plugin + ImageJ by command line.
 * Example:
 * <pre>java -jar dbv-project-streetlamp-0.0.1-SNAPSHOT-jar-with-dependencies.jar -i=/Users/Tu/Desktop/dbv/examples/image_3.png</pre> 
 * */
public class PlugInStarter
{
	public static final String PLUGIN_PROJECT = "DbvProjekt ";

	public static void main(String[] args)
	{	
		StringBuilder builder = new StringBuilder();
		for(String s : args)
		{
			builder.append(s + " ");
		}
		
		IJ.runPlugIn("DbvProjekt_",builder.toString());
//		ImageJ.main(args);
	}

}
