import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.Roi;
import ij.gui.TextRoi;
import ij.measure.Measurements;
import ij.measure.ResultsTable;
import ij.plugin.ChannelSplitter;
import ij.plugin.ContrastEnhancer;
import ij.plugin.PlugIn;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.frame.RoiManager;
import ij.process.ByteStatistics;
import ij.process.ColorProcessor;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;


public class DbvProjekt_ implements PlugIn
{
//	private static final String	DIRECTORY_TO_FILE		= System.getProperty("user.home") + "/Desktop/dbv/examples/";

	private static final String	FILE_NAME				= "image_1.jpg";

	private static final double	MAX_WIDTH_HEIGHT_RATIO	= 1.6;

	private static final double	MIN_WIDTH_HEIGHT_RATIO	= 0.60;

	private static final double	GAUSSIAN_BLUR			= 2.0;

	private static final int	MIN_LIGHT_RGB_VALUE		= 250;

	private static final double	COLOR_FILTER_B_TO_R		= 0.6;

	private static final double	COLOR_FILTER_B_TO_G		= 0.6;

	private static final int	DRAW_BOX_EDGE			= 50;

	private static final int	MIN_ANALYZE_AREA		= 20;

	private static final int	GREY_VALUE_THRESHOLD	= 210;

	private static final double	VERTICAL_CROP_RATIO		= 0.6;

//	public static final String	PATH_TO_FILE			= DIRECTORY_TO_FILE + FILE_NAME;

	private static final Color	BOX_STROKE_COLOR		= new Color(0, 255, 255);										// CYAN

	private Rectangle			DRAW_BOX;

	private String				_inputFile;

	private String				_outputDirectory;

	private Integer				_greyValueThreshold;

	private Double				_gaussianBlur;

	private Double				_verticalCropRatio;

	private Integer				_minAnalyzeArea;

	private Integer				_drawBoxEdge;

	private Double				_colorFilterB2G;

	private Double				_colorFilterB2R;

	private Integer				_minLightRgbValue;

	private Double				_minWidthHeightRatio;

	private Double				_maxWidthHeightRatio;

	private Boolean				_showSteps;

	@Override
	public void run(String arg)
	{
		setParams(arg);
		DRAW_BOX = new Rectangle(0, 0, _drawBoxEdge, _drawBoxEdge);

		ImagePlus originalImage = null;

		if ( WindowManager.getIDList() != null && WindowManager.getIDList().length > 0 )
		{
			originalImage = WindowManager.getImage(WindowManager.getIDList()[0]);
		}
		else
		{
			originalImage = new ImagePlus(_inputFile);
		}

		double height = _verticalCropRatio * originalImage.getHeight();
		int heightInt = (int) height;
		originalImage.setRoi(0, 0, originalImage.getWidth(), heightInt);

		ColorProcessor resultProcessor = new ColorProcessor(originalImage.getImage());
		List<Roi> foundStreetLamps = processingFindingObjects(originalImage.duplicate(), _greyValueThreshold, _minAnalyzeArea,
		        ObjectMatching.LIGHTSOURCE);
		ColorProcessor colorFilterProcessor = new ColorProcessor(originalImage.getImage());
		
		ContrastEnhancer contrastEnhancer = new ContrastEnhancer();
		contrastEnhancer.equalize(colorFilterProcessor);

		List<Roi> filteredStreetLamps = filterRoisByColor(foundStreetLamps, colorFilterProcessor);

		for ( Roi streetLampObject : filteredStreetLamps )
		{
			Rectangle rect = streetLampObject.getBounds();
			DRAW_BOX.setLocation(
			        new Point((int) rect.getCenterX() - (int) DRAW_BOX.getWidth() / 2, (int) rect.getCenterY() - (int) DRAW_BOX.getHeight() / 2));
			Roi toDrawRoi = new Roi(DRAW_BOX);
			toDrawRoi.setStrokeColor(BOX_STROKE_COLOR);

			TextRoi indexIndicator = new TextRoi(rect.getCenterX(), rect.getCenterY(), String.valueOf(foundStreetLamps.indexOf(streetLampObject)));
			resultProcessor.drawRoi(toDrawRoi);
			resultProcessor.draw(indexIndicator);
		}

		String baseFileName = FILE_NAME.replaceAll("(.*)\\.\\w+", "$1");
		ImagePlus resultImage = new ImagePlus(baseFileName, resultProcessor);
		IJ.saveAs(resultImage, "JPG", _outputDirectory + baseFileName + "_RoadLights");
		
		if ( _showSteps )
		{
			resultImage.show();
		}

		try
		{
			XmlDataWriter.writeXml(_outputDirectory, FILE_NAME, filteredStreetLamps);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private List<Roi> filterRoisByColor(List<Roi> foundStreetLamps, ColorProcessor colorFilterProcessor)
	{
		List<Roi> filteredRoi = new ArrayList<Roi>();

		for ( Roi streetLampObject : foundStreetLamps )
		{
			Rectangle rect = streetLampObject.getBounds();
			colorFilterProcessor.setRoi(rect);

			ImageProcessor colorFilter = colorFilterProcessor.crop();
			ImagePlus lightSouceImage = new ImagePlus("lightsource", colorFilter);

			ImagePlus[] colorComponents = ChannelSplitter.split(lightSouceImage);

			boolean isRoadLight = true;
			List<ByteStatistics> statistics = new ArrayList<ByteStatistics>();
			for ( int i = 0; i < colorComponents.length; i++ )
			{
				ByteStatistics stats = new ByteStatistics(colorComponents[i].getProcessor());
				statistics.add(stats);
			}

			isRoadLight = validateStatistics(statistics);

			if ( isRoadLight )
			{
				filteredRoi.add(streetLampObject);
			}
		}

		return filteredRoi;
	}

	private boolean validateStatistics(List<ByteStatistics> statistics)
	{
		ByteStatistics red = statistics.get(0);
		ByteStatistics green = statistics.get(1);
		ByteStatistics blue = statistics.get(2);

		// first we want to validate if red and green ratio is not too high
		if ( (blue.min / green.min) > _colorFilterB2G )
		{
			return false;
		}

		if ( (blue.min / red.min) > _colorFilterB2R )
		{
			return false;
		}

		if ( (red.max < _minLightRgbValue) && (green.max < _minLightRgbValue) && (blue.max < _minLightRgbValue) )
		{
			return false;
		}

		if ( (red.min == 0) || (green.min == 0) )
		{
			return false;
		}

		return true;
	}

	private List<Roi> processingFindingObjects(ImagePlus processImage, int threshold, int minObjRadius, ObjectMatching targetObj)
	{
		List<Roi> ret = new ArrayList<Roi>();

		ContrastEnhancer contrastEnhancer = new ContrastEnhancer();
		contrastEnhancer.equalize(processImage);

		if ( _showSteps )
		{
			new ImagePlus("contrast", processImage.getImage()).show();;
		}

		ImageConverter converter = new ImageConverter(processImage);
		converter.convertToGray8();

		ImageProcessor eightBitProcessor = processImage.getProcessor();
		eightBitProcessor.blurGaussian(_gaussianBlur);
		eightBitProcessor.threshold(threshold);
		if(_showSteps)
		{
			(new ImagePlus("threshold", eightBitProcessor)).show();
		}
		
		eightBitProcessor.invert();

		ResultsTable analyzerResults = new ResultsTable();
		ParticleAnalyzer analyzer = new ParticleAnalyzer(ParticleAnalyzer.ADD_TO_MANAGER, Measurements.LABELS, analyzerResults, minObjRadius,
		        eightBitProcessor.getWidth() * eightBitProcessor.getHeight());
		ImagePlus particleAnalyzeImage = new ImagePlus("particle_analyze", eightBitProcessor);

		if(_showSteps)
		{
			particleAnalyzeImage.show();
		}

		if ( analyzer.analyze(particleAnalyzeImage) )
		{
			RoiManager roiManager = RoiManager.getInstance();
			if(!_showSteps)
			{
				roiManager.close();
			}

			for ( int i = 0; i < roiManager.getRoisAsArray().length; i++ )
			{
				Roi roi = roiManager.getRoisAsArray()[i];

				Rectangle boundingBox = roi.getBounds();
				eightBitProcessor.setRoi(boundingBox);

				ImageProcessor cropProcessor = eightBitProcessor.crop();
				if ( roi != null && (!(((double) cropProcessor.getWidth() / (double) cropProcessor.getHeight()) < _minWidthHeightRatio)
				        && !(((double) cropProcessor.getWidth() / (double) cropProcessor.getHeight()) > _maxWidthHeightRatio)) )
				{
					ret.add(roi);
				}
			}
		}

		return ret;
	}

	enum ObjectMatching
	{
		LIGHTSOURCE, STREETLAMP;
	}

	private void setParams(String args) throws IllegalArgumentException
	{
		// overide
		_verticalCropRatio = VERTICAL_CROP_RATIO;
		_greyValueThreshold = GREY_VALUE_THRESHOLD;
		_minAnalyzeArea = MIN_ANALYZE_AREA;
		_gaussianBlur = GAUSSIAN_BLUR;
		_drawBoxEdge = DRAW_BOX_EDGE;
		_colorFilterB2G = COLOR_FILTER_B_TO_G;
		_colorFilterB2R = COLOR_FILTER_B_TO_R;
		_minLightRgbValue = MIN_LIGHT_RGB_VALUE;
		_minWidthHeightRatio = MIN_WIDTH_HEIGHT_RATIO;
		_maxWidthHeightRatio = MAX_WIDTH_HEIGHT_RATIO;
		_showSteps = false;

		// params are splitted by space
		if ( args == null || args.isEmpty() )
		{
			return;
		}

		for ( String s : args.split("\\s") )
		{
			if ( s.contains("=") )
			{
				String[] keyValuePair = s.split("=");
				if ( keyValuePair.length == 2 )
				{
					String key = keyValuePair[0];
					String value = keyValuePair[1];

					if ( "-i".equals(key) )
					{
						_inputFile = value;
						if ( _inputFile.startsWith("~") )
						{
							_inputFile = _inputFile.replaceAll("\\~", System.getProperty("user.home"));
						}

						// set default output directory
						_outputDirectory = _inputFile.replaceAll("(.*/).*\\.*$", "$1");
					}
					if("-o".equals(key))
					{
						_outputDirectory = value;
					}
					if ( "-verticalCropRatio".equals(key) )
					{
						_verticalCropRatio = Double.valueOf(value);
					}
					if ( "-greyValueThreshold".equals(key) )
					{
						_greyValueThreshold = Integer.valueOf(value);
					}
					if ( "-minAnalyzeArea".equals(key) )
					{
						_minAnalyzeArea = Integer.valueOf(value);
					}
					if ( "-gaussianBlur".equals(key) )
					{
						_gaussianBlur = Double.valueOf(value);
					}
					if ( "-drawBoxEdge".equals(key) )
					{
						_drawBoxEdge = Integer.valueOf(value);
					}
					if ( "-colorFilterB2G".equals(key) )
					{
						_colorFilterB2G = Double.valueOf(value);
					}
					if ( "-colorFilterB2R".equals(key) )
					{
						_colorFilterB2R = Double.valueOf(value);
					}
					if ( "-minLightRgbValue".equals(key) )
					{
						_minLightRgbValue = Integer.valueOf(value);
					}
					if ( "-minWidthHeightRatio".equals(key) )
					{
						_minWidthHeightRatio = Double.valueOf(value);
					}
					if ( "-maxWidthHeightRatio".equals(key) )
					{
						_maxWidthHeightRatio = Double.valueOf(value);
					}
				}
			}
			else
			{
				if ( "-showSteps".equals(s) )
				{
					_showSteps = true;
				}
			}
		}
		
		
		if(_inputFile == null)
		{
			throw new IllegalArgumentException();
		}
	}
}
