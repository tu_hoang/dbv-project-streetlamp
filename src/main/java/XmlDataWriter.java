import ij.gui.Roi;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class XmlDataWriter
{
	public static void writeXml(String pathToFile, String fileName, List<Roi> foundRoadLights) throws ParserConfigurationException,
	        TransformerException, UnsupportedEncodingException
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuiler = docFactory.newDocumentBuilder();

		// root
		Document rootDoc = docBuiler.newDocument();
		Element rootElement = rootDoc.createElement("video");
		rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		rootElement.setAttribute("xsi:noNamespaceSchemaLocation", "objectDetection.xsd");
		rootDoc.appendChild(rootElement);

		// info
		Element infoElement = rootDoc.createElement("info");
		rootElement.appendChild(infoElement);

		// file
		Element fileElement = rootDoc.createElement("file");
		fileElement.setAttribute("type", "file");
		fileElement.appendChild(rootDoc.createTextNode(pathToFile + fileName));
		infoElement.appendChild(fileElement);

		// id
		Element idElement = rootDoc.createElement("id");
		Integer randomId = (int) (10000.0 * Math.random());
		// because generating a character id is too complicated
		idElement.appendChild(rootDoc.createTextNode(String.valueOf(randomId)));
		infoElement.appendChild(idElement);

		// name
		Element nameElement = rootDoc.createElement("name");
		nameElement.appendChild(rootDoc.createTextNode("RoadLights"));
		infoElement.appendChild(nameElement);

		// description

		// framecount

		// frames
		Element framesElement = rootDoc.createElement("frames");
		rootElement.appendChild(framesElement);

		// frame
		Element frameElement = rootDoc.createElement("frame");
		framesElement.appendChild(frameElement);

		// objects
		Element objectsElement = rootDoc.createElement("objects");
		frameElement.appendChild(objectsElement);

		// all objects
		for ( Roi roi : foundRoadLights )
		{
			Element objectElement = rootDoc.createElement("object");
			objectElement.setAttribute("id", fileName + foundRoadLights.indexOf(roi));
			objectsElement.appendChild(objectElement);

			Element rectElement = rootDoc.createElement("shape");
			rectElement.setAttribute("typ", "rectangle");
			objectElement.appendChild(rectElement);

			Element widthElement = rootDoc.createElement("width");
			widthElement.appendChild(rootDoc.createTextNode(String.valueOf(roi.getBounds().getWidth())));
			rectElement.appendChild(widthElement);

			Element heightElement = rootDoc.createElement("height");
			heightElement.appendChild(rootDoc.createTextNode(String.valueOf(roi.getBounds().getHeight())));
			rectElement.appendChild(heightElement);

			Element positionElement = rootDoc.createElement("position");
			rectElement.appendChild(positionElement);

			Element xPositionElement = rootDoc.createElement("x");
			xPositionElement.appendChild(rootDoc.createTextNode(String.valueOf((int) roi.getBounds().getX())));
			positionElement.appendChild(xPositionElement);
			
			Element yPositionElement = rootDoc.createElement("y");
			yPositionElement.appendChild(rootDoc.createTextNode(String.valueOf((int) roi.getBounds().getY())));
			positionElement.appendChild(yPositionElement);
			
		}

		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		DOMSource source = new DOMSource(rootDoc);

		String baseFileName = fileName.replaceAll("(.*)\\.\\w+", "$1");
		StreamResult result = new StreamResult(new File(pathToFile + baseFileName + "_RoadLights.xml"));
		transformer.transform(source, result);
	}

}
